import React from "react";
import dayjs from "dayjs";
import { Line } from "react-chartjs-2";

import socketConsume from "./app/Utils/socketConsume";
import Button from "./app/Commons/Button";
import Table from "./app/Commons/Table";

const chartConfig = {
  labels: [],
  datasets: [
    {
      label: "Grafik Data Sensor",
      fill: false,
      lineTension: 0.1,
      backgroundColor: "rgba(75,192,192,0.4)",
      borderColor: "rgba(75,192,192,1)",
      borderCapStyle: "butt",
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: "miter",
      pointBorderColor: "rgba(75,192,192,1)",
      pointBackgroundColor: "#fff",
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: "rgba(75,192,192,1)",
      pointHoverBorderColor: "rgba(220,220,220,1)",
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: []
    }
  ]
};

const App = () => {
  const [power, setPower] = React.useState(false);
  const [logger, setLogger] = React.useState([]);
  const [chartData, setChartData] = React.useState(chartConfig);

  React.useEffect(() => {
    let newLabels = [];
    let newData = [];

    power &&
      socketConsume("event_name", data => {
        setLogger(oldArray => [...oldArray, data]);
        setChartData(oldArray => {
          let chartLoggerLabels = oldArray.labels;
          let chartDatasets = oldArray.datasets[0];
          let chartLoggerData = chartDatasets.data;

          newLabels = [
            ...chartLoggerLabels,
            dayjs(data.time).format("HH:mm:ss")
          ];
          newData = [...chartLoggerData, data.data.toFixed(2)];

          return {
            labels: newLabels.length > 5 ? newLabels.slice(1, 6) : newLabels,
            datasets: [
              {
                ...chartDatasets,
                data: newData.length > 5 ? newData.slice(1, 6) : newData
              }
            ]
          };
        });
      });
  }, [power]);

  return (
    <div style={{ display: "flex", padding: 20 }}>
      <div style={{ flex: 1 }}>
        <h3>
          Power : <span>{power ? "on" : "off"}</span>
        </h3>
        <Button
          color={power ? "red" : "green"}
          onClick={() => setPower(!power)}
        >
          {power ? "Matikan" : "Nyalakan"}
        </Button>

        <h3>Log Data Sensor</h3>
        <Table
          config={{ heading: [{ name: "Waktu" }, { name: "Nilai" }] }}
          data={logger}
        />
      </div>

      <div style={{ flex: 1 }}>
        <Line data={chartData} />
      </div>
    </div>
  );
};

export default App;
