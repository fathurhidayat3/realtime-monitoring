import styled from "styled-components";

const Button = styled.button`
  padding: 4px 8px;
  background: ${props => props.color && props.color};
  color: white;
  border: none;
`;

export default Button;
