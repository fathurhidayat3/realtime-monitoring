import socketIOClient from "socket.io-client";

const socket = socketIOClient(process.env.REACT_APP_SERVER_ADDRESS);

const socketConsume = (channelName, callback) => {
  socket.on(channelName, res => callback({ time: new Date(), data: res }));
};

export default socketConsume;
