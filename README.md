# Realtime Monitoring App

# Installation :

1. point to this project directory
2. execute `yarn install:dummy`
3. then to run use `yarn run:dummy`

# Separate Installation :

### Client

1. point to this project directory
2. execute `yarn`
3. to run client app `yarn start`

### Server

1. still at this project directory
2. then move using `cd server`
3. install deps `yarn`
4. run server with `node index.js`
