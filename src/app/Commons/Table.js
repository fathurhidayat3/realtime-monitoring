import React from "react";
import dayjs from "dayjs";
import styled from "styled-components";

const StyledTable = styled.table`
  width: 400px;
  table-layout: fixed;
  border-collapse: collapse;
`;

const StyledTbody = styled.table`
  display: block;
  height: 300px;
  width: 100%;
  overflow: auto;
`;

const StyledTh = styled.th`
  width: 200px;
  text-align: left;
`;
const StyledTd = styled(StyledTh)``;

const Table = ({ data, config }) => {
  const { heading } = config;

  return (
    <StyledTable>
      <thead>
        <tr style={{ display: "block" }}>
          {heading.map(headingItem => {
            return <StyledTh>{headingItem.name}</StyledTh>;
          })}
        </tr>
      </thead>
      <StyledTbody>
        {data.map((dataItem, idx) => {
          return (
            <tr key={idx}>
              <StyledTd>
                {dayjs(dataItem.time).format("DD/MM/YYYY HH:mm:ss")}
              </StyledTd>
              <StyledTd>{dataItem.data.toFixed(2)}</StyledTd>
            </tr>
          );
        })}
      </StyledTbody>
    </StyledTable>
  );
};

export default Table;
