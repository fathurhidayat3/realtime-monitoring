require("dotenv").config();

const express = require("express");
const http = require("http");
const socketIo = require("socket.io");
const axios = require("axios");
const port = process.env.REACT_APP_SERVER_PORT;
const index = require("./routes/index");
const app = express();

app.use(index);

const server = http.createServer(app);
const io = socketIo(server);

let interval;

const getApiAndEmit = async socket => {
  try {
    const res = await axios.get(
      `https://api.darksky.net/forecast/${process.env.REACT_APP_DARKSKY_API_KEY}/43.7695,11.2558`
    );
    socket.emit("FromDevice", res.data.currently.temperature);

    console.log(`Temp : ${res.data.currently.temperature}`);
  } catch (error) {
    console.error(`Error: ${error}`);
  }
};

io.on("connection", socket => {
  console.log("New client connected");
  if (interval) {
    clearInterval(interval);
  }
  interval = setInterval(() => getApiAndEmit(socket), 1000);
  socket.on("disconnect", () => {
    console.log("Client disconnected");
  });
});

server.listen(port, () => console.log(`Listening on port ${port}`));
